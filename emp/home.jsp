<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="employee" class="com.jiffy.bean.EmployeeDetails" scope="session" />
<%@include file="../include/verifySession.jsp" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>:: JIFFY ::</title>
		<link href="../css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<form name="frmHome">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr><td><%@include file="../include/header.jsp" %></td></tr>
				<tr><td><%@include file="../include/menu.jsp" %></td></tr>
			</table>
		</form>
	</body>
</html>