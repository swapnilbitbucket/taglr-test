<%@page import="com.jiffy.util.DateTime"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="employee" class="com.jiffy.bean.EmployeeDetails" scope="session" />
<%@include file="../include/verifySession.jsp" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>:: JIFFY ::</title>
		<link href="../css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="../js/XMLHttpRequest.js"></script>
		<script type="text/javascript" src="../js/attendanceList.js"></script>
	</head>
	<% 
		try{
			
			Date date 			= new Date();
			int month 			= (date.getMonth());
			int year  			= (date.getYear())+1900; //starts from 1900..
			
			Date doj 			= employee.getDOJ();
			String tempDOJ[] 	= (doj+"").split("-"); 
			int monthOfDOJ 		= Integer.parseInt(tempDOJ[1]);
			int yearOfDOJ  		= Integer.parseInt(tempDOJ[0]);
			
			String cmbMonth 	= ReplaceNull.replace(request.getParameter("cmbMonth"));
			String cmbYear 		= ReplaceNull.replace(request.getParameter("cmbYear"));
			
			// month and year selected from drop down
			if(cmbMonth.length()>0){
				month = Integer.parseInt(cmbMonth);
			}
			if(cmbYear.length()>0){
				year = Integer.parseInt(cmbYear);
			}
			
			%>
			<body onload="fncPopulateList();">
				<form name="frmAttendanceList">
					<input type="hidden" name="userId" value="<%= employee.getUserId() %>">
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr><td><%@include file="../include/header.jsp" %></td></tr>
						<tr><td><%@include file="../include/menu.jsp" %></td></tr>
						<tr>
							<td align="center">
								<%
									String msg = ReplaceNull.replace((String)session.getAttribute("msg"));
									session.removeAttribute("msg");
								%>
								<font color="red"><%= msg %></font>
							</td>
						</tr>
						<tr>
							<td align="center">
								<table>
									<tr align="center">
										<td>Month :
											<select name="cmbMonth" onchange="fncPopulateList();"> 
												<%for(int i=0;i<=11;i++){%>
													<option value="<%=i%>" <%= month==i ? "selected" : "" %> ><%=DateTime.getMonthName(i)%></option>
												<%}%>
											</select>
										</td>
										<td>Year :
											<select name="cmbYear" onchange="fncPopulateList();"> 
												<%for(int i=yearOfDOJ;i<=year;i++){%>
													<option value="<%=i%>" <%= year==i ? "selected" : "" %> ><%=i%></option>
												<%}%>
											</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr width="60%" align="center">
							<td>
								<%@include file="../include/attendanceList.jsp" %>
							</td>
						</tr>
					</table>
				</form>
			</body>
			<%
		}catch(Exception e){
			System.out.println("Exception in attendanceList.jsp :: "+e);
			//e.printStackTrace();
		}
	%>
</html>