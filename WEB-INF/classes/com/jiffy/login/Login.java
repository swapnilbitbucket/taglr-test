package com.jiffy.login;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jiffy.constant.DBConnection;
import com.jiffy.constant.ReplaceNull;
import com.jiffy.db.dao.EmployeeLogin;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doProcess(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		String userid 	= ReplaceNull.replace(request.getParameter("txtUserid"));
		String password = ReplaceNull.replace(request.getParameter("txtPassword"));
		
		DBConnection db 		= new DBConnection();
		Connection conn =null;
		try{
			conn = db.getDBConnection(getServletContext());
			if(EmployeeLogin.isValidEmployeeLogin(conn, userid, password)){
				session.setAttribute("userid", userid);
				response.sendRedirect("Redirect");
				return;
			}else{
				session.setAttribute("msg", "User ID or Password Incorrect");
				response.sendRedirect("login");
				return;
			}			
		}catch(Exception e){
			System.out.println("Exception in Login.java :: "+e);
			//e.printStackTrace();
		}finally{
			db.closeConnection(conn);
			db=null;
		}		
	}
}
