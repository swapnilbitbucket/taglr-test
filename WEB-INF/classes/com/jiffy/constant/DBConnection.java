package com.jiffy.constant;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;

public class DBConnection extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public Connection getDBConnection(ServletContext context){
		Connection conn=null;		
		
		// getting all the information from the Context-Param
		String DRIVER = context.getInitParameter("DRIVER");
		String URL = context.getInitParameter("URL");
		String DB_NAME = context.getInitParameter("DATABASE_NAME");
		String USERNAME = context.getInitParameter("DATABASE_USERNAME");
		String PASSWORD = context.getInitParameter("DATABASE_PASSWORD");
		
		try{
			//Register JDBC driver
			Class.forName(DRIVER);
			
			//Open a connection
		    conn = DriverManager.getConnection(URL+DB_NAME+"?useSSL=false",USERNAME,PASSWORD);
			
		}catch(Exception e){
			System.out.println("Exception in DBConnection :: "+e);
		}
		
		return conn;
	}
	
	public void closePreparedStatement(PreparedStatement pstmt){
		try{
			if(pstmt != null){
				pstmt.close();
			}
		}catch(Exception e){}
	}
	
	public void closeStatement(Statement stmt){
		try{
			if(stmt != null){
				stmt.close();
			}
		}catch(Exception e){}
	}
	
	public void closeResultSet(ResultSet rs){
		try{
			if(rs != null){
				rs.close();
			}
		}catch(Exception e){}
	}
	
	public void closeConnection(Connection conn){
		try{
			if(conn != null){
				conn.close();
			}
		}catch(Exception e){}
	}
	
	public void closeCallableStatement(CallableStatement cstmt){
		try{
			if(cstmt != null){
				cstmt.close();
			}
		}catch(Exception e){}
	}

}
