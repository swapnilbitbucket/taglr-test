package com.jiffy.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.jiffy.bean.EmployeeDetails;
import com.jiffy.bean.SaveBean;
import com.jiffy.constant.DBConnection;
import com.jiffy.constant.ReplaceNull;
import com.jiffy.db.dao.SaveTimeDao;

/**
 * Servlet implementation class SaveTime
 */
public class SaveTime extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveTime() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}
	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DBConnection db = new DBConnection();
		Connection conn = null;
		PreparedStatement pstmt=null;
		ResultSet rs = null;
		try{
			HttpSession session = request.getSession();
			
			conn = db.getDBConnection(getServletContext());
			
			String day 		= ReplaceNull.replace(request.getParameter("day"));
			String month 	= ReplaceNull.replace(request.getParameter("month"));
			String year 	= ReplaceNull.replace(request.getParameter("year"));
			
			String startHour 	= ReplaceNull.replace(request.getParameter("startHour"));
			String startMinute 	= ReplaceNull.replace(request.getParameter("startMinute"));
			String endHour 		= ReplaceNull.replace(request.getParameter("endHour"));
			String endMinute 	= ReplaceNull.replace(request.getParameter("endMinute"));
			
			EmployeeDetails employee = (EmployeeDetails) session.getAttribute("employee");
			
			String userId = employee.getUserId();
			
			SaveBean saveTime = new SaveBean();
			saveTime.setDay(day);
			saveTime.setMonth(month);
			saveTime.setYear(year);
			saveTime.setStartHour(startHour);
			saveTime.setStartMinute(startMinute);
			saveTime.setEndHour(endHour);
			saveTime.setEndMinute(endMinute);
			saveTime.setUserId(userId);
			
			SaveTimeDao save = new SaveTimeDao();
			
			boolean b = save.saveTimeInDB(conn,pstmt,rs,saveTime);
			if(b){
				session.setAttribute("msg", "Time Save Successfully.");
			}else{
				session.setAttribute("msg", "Please Try Again.");
			}
			response.sendRedirect("list");
			return;
		}catch(Exception e){
			System.out.println("Exception in SaveTime.java :: "+e);
			//e.printStackTrace();;
		}finally{
			db.closeResultSet(rs);
			db.closePreparedStatement(pstmt);
			db.closeConnection(conn);
			db=null;
		}
	}

}
