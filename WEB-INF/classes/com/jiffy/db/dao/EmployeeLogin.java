package com.jiffy.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.jiffy.constant.DBConnection;

public class EmployeeLogin {
	public static boolean isValidEmployeeLogin(Connection conn,String userid,String password){
		boolean isValidLogin	= false;
		PreparedStatement pstmt = null;
		DBConnection db			= new DBConnection();
		ResultSet rs 			= null;
		try{			
			String query = "select * from USER_MASTER where user_id=? and password=?";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, userid);
			pstmt.setString(2, password);
			rs = pstmt.executeQuery();
			if(rs.next()){
				isValidLogin = true;
			}else{
				isValidLogin = false;
			}rs.close();pstmt.close();
		}catch(Exception e){
			System.out.println("Exception in EmployeeLogin >> isValidEmployeeLogin :: "+e);
			//e.printStackTrace();
		}finally{
			db.closePreparedStatement(pstmt);
			db.closeResultSet(rs);
		}
		
		return isValidLogin;
	}
}
