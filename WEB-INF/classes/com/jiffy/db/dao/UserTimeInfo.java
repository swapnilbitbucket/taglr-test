package com.jiffy.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import javax.servlet.ServletContext;

import com.jiffy.bean.SaveBean;
import com.jiffy.constant.DBConnection;
import com.jiffy.constant.ReplaceNull;

public class UserTimeInfo {

	public HashMap<String, HashMap<Integer,SaveBean>> getUserTimeInfo(ServletContext context, String userId,int month,int year){
		
		HashMap<String, HashMap<Integer,SaveBean>> saveBeanMap = new HashMap<String,HashMap<Integer,SaveBean>>();
		
		DBConnection db = new DBConnection();
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try{
			conn = db.getDBConnection(context);
			HashMap<Integer,SaveBean> tempSaveBean = new HashMap<Integer,SaveBean>();
			String query = " select * from USER_TIMESHEET where user_id=? and month=? and year=? ";
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, userId);
			pstmt.setString(2, month+"");
			pstmt.setString(3, year+"");
			rs = pstmt.executeQuery();
			while(rs.next()){
				SaveBean saveBean = new SaveBean();
				saveBean.setUserId(ReplaceNull.replace(rs.getString("user_id")));
				saveBean.setDay(ReplaceNull.replace(rs.getString("day")));
				saveBean.setMonth(ReplaceNull.replace(rs.getString("month")));
				saveBean.setYear(ReplaceNull.replace(rs.getString("year")));
				saveBean.setStartHour(ReplaceNull.replace(rs.getString("start_hour")));
				saveBean.setStartMinute(ReplaceNull.replace(rs.getString("start_minute")));
				saveBean.setEndHour(ReplaceNull.replace(rs.getString("end_hour")));
				saveBean.setEndMinute(ReplaceNull.replace(rs.getString("end_minute")));
				
				tempSaveBean.put(rs.getInt("day"), saveBean);
			}rs.close();pstmt.close();
			
			saveBeanMap.put(userId, tempSaveBean);
			
		}catch(Exception e){
			System.out.println("Exception in UserTimeInfo.java "+e);
			//e.printStackTrace();
		}finally{
			db.closeResultSet(rs);
			db.closePreparedStatement(pstmt);
			db.closeConnection(conn);
			db=null;
		}
		
		return saveBeanMap;
	}
}
