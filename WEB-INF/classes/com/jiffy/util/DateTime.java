package com.jiffy.util;

import java.util.Date;
import java.text.SimpleDateFormat;

public class DateTime {
	public static int getNumberOfDaysInMonth(int month,int year){
		int days=0;
		boolean isLeapYear = isLeapYear(year);
		
		// month should be in between january and december
		if(!(month<Constant.JANUARY && month>Constant.DECEMBER)){
			switch(month){
				case Constant.FEBRUARY:
					days=isLeapYear ? 29 : 28;
					break;
				case Constant.APRIL:
				case Constant.JUNE:
				case Constant.SEPTEMBER:
				case Constant.NOVEMBER:
					days = 30;
					break;
				default:
					days = 31;
					break;
			}
		}
		return days;
	}
	
	public static boolean isLeapYear(int year){
		return year%4 == 0 ? true : false;
	}
	
	public static String getMonthName(int month){
		String []monthList 	= {"January","February","March","April","May","June","July","August","September","October","November","December"};
		return monthList[month];
	}
	
	public static String getDayName(int day,int month,int year){
		String date = day+"/"+month+"/"+year;
		SimpleDateFormat newDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date MyDate = null;
		try{
			MyDate = newDateFormat.parse(date);
			newDateFormat.applyPattern("EEEE");			
		}catch(Exception e){
			System.out.println("Exception in DateTime >> getDayName :: "+e);
			//e.printStackTrace();
		}
		return newDateFormat.format(MyDate);
	}

}
