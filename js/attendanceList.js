function fncPopulateList(flag)
{
	var month 	= document.forms[0].cmbMonth.value;
	var year  	= document.forms[0].cmbYear.value;
	var userId  = document.forms[0].userId.value;

	var url   = "../include/populateAttendanceList.jsp?month="+month+"&year="+year+"&flag="+flag+"&userId="+userId;
	
	var xmlHttpRequest = getXMLHttpRequest();
	
	xmlHttpRequest.onreadystatechange = fncCallBackPopulateList(xmlHttpRequest);
	xmlHttpRequest.open("POST", url, true);
	xmlHttpRequest.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	xmlHttpRequest.send(url);
	
}
function fncCallBackPopulateList(xmlHttpRequest) {
	return function() {
		if (xmlHttpRequest.readyState == 4) {
			if (xmlHttpRequest.status == 200) {
				document.getElementById("attendanceListID").innerHTML = xmlHttpRequest.responseText;
			} else {
				alert("HTTP error " + xmlHttpRequest.status + ": " + xmlHttpRequest.statusText);
			}
		}
	};
}

function fncSubmit()
{
	document.forms[0].method="Post";
	document.forms[0].action="AddTask";
	document.forms[0].submit();
}

function fncShowEndTime(day)
{	
	if((document.getElementById("cmbStartHour"+day).value != "") && (document.getElementById("cmbStartMinute"+day).value != "")){
		document.getElementById("btnStartOk"+day).disabled=false;
		document.getElementById("cmbEndHour"+day).disabled=false;
		document.getElementById("cmbEndMinute"+day).disabled=false;
		document.getElementById("btnEndOk"+day).disabled=false;
	}else{
		document.getElementById("btnStartOk"+day).disabled=true;
		document.getElementById("cmbEndHour"+day).disabled=true;
		document.getElementById("cmbEndMinute"+day).disabled=true;
		document.getElementById("btnEndOk"+day).disabled=true;
	}
}

function fncSaveTime(day,month,year)
{
	var startHour 	= document.getElementById("cmbStartHour"+day).value;
	var startMinute = document.getElementById("cmbStartMinute"+day).value;
	var endHour 	= document.getElementById("cmbEndHour"+day).value;
	var endMinute 	= document.getElementById("cmbEndMinute"+day).value;
		
	var url ="SaveTime?day="+day+"&month="+month+"&year="+year+"&startHour="+startHour+"&endHour="+endHour+"&startMinute="+startMinute+"&endMinute="+endMinute;
	document.forms[0].method="Post";
	document.forms[0].action=url;
	document.forms[0].submit();
}
